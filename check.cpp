//
// Created by SrSyntaxAT | Marcel H. on 10.08.2020. All rights reserved.
//

#include "check.h"

#include <iterator>

void tweet_analytics::check::check_text(std::string text) {
    //std::cout << "Check: " << text << std::endl;
    std::string word_in_text;

    for (unsigned short i = 0; i < text.size(); i++) {
        char character = text.at(i);

        if (character == ' ') {
            //std::cout << "Word: " << b << std::endl;
            const auto it = word_position.find(word_in_text);
            word* word1;

            if (it == word_position.end()) {
                word1->started = CURRENT_TIME;
                words.push_back(word1);
                word_position.insert(pair<string, int>(word_in_text, words.size() - 1));

                //std::cout << "Add word: " << word_in_text << std::endl;
            } else word1 = words.at(it->second);

            const std::chrono::milliseconds since = CURRENT_TIME - word1->started;

            if (since.count() >= (60 * 60 * 1000)) {
                word1->started = CURRENT_TIME;
                word1->mentions = 0;
            }
            word1->mentions++;

            //std::cout << word_in_text << " \t: " << word1->mentions << std::endl;

            if (word1->mentions >= FIRE) {
                //std::cout << "FIRE:\t" << word_in_text << std::endl;
            }

            word_in_text.clear();
            continue;
        } else word_in_text += tolower(character);
    }
}
