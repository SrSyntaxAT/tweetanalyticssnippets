//
// Created by SrSyntaxAT | Marcel H. on 10.08.2020. All rights reserved.
//

#ifndef TWEETANALYTICS_CHECK_H
#define TWEETANALYTICS_CHECK_H

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <chrono>

namespace tweet_analytics {

#define FIRE 500

#define CURRENT_TIME std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch())

    using namespace std;

    struct word {
        std::chrono::milliseconds started{};
        unsigned int mentions = 0;
    };

    class check {
    private:
        map<string, unsigned int> word_position{};
        vector<word*> words;
    public:
        void check_text(string);
    };
}

#endif //TWEETANALYTICS_CHECK_H
